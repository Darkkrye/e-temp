E-Temp' Project
========================

Welcome to the E-Temp' Project
The following site is written with PHP and Symfony3 Framework

Used technologies are : HTML(Twig), CSS(Bootstrap), JavaScript(jQuery), PHP(Symfony3)

What's inside ?
--------------

All of the files needed for the project : 

  * A full Symfony3 (3.0.0) Project

  * Specific CSS for the ephemeral theme.


What's the content ?
--------------

  * The content is a website with an ephemeral theme.

  * Multiple CSS for different theme
  


ENJOY !!!