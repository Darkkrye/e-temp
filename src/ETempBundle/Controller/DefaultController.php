<?php

namespace ETempBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ETempBundle:Default:index.html.twig');
    }
}
